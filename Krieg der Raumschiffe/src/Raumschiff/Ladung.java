package Raumschiff;

public class Ladung {

	private int anzahl;
	private String bezeichnung;
	

	public Ladung() {

	}
 
	public Ladung(String bezeichnung, int anzahl) {
		setBezeichnung(bezeichnung);
		setAnzahl(anzahl);
	}
	public int getAnzahl() {
		return anzahl;
	}

	public void setAnzahl(int zaehlung) {
		this.anzahl = zaehlung;
	}

	public String getBezeichnung() {
		return bezeichnung;
	}

	public void setBezeichnung(String name) {
		this.bezeichnung = name;
	}

	

	public void LadungEntleeren () {
		this.bezeichnung =null;
		this.anzahl=0; 
	}


}
