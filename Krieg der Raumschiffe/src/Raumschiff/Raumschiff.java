package Raumschiff;
import java.util.ArrayList;

public class Raumschiff {

	private int anzahlTorpedoes;
	private int energieversorgungInProzent;
	private int huelleInProzent;
	private int schutzschildInProzent;
	private int lebenserhaltungssystemeInProzent;
	private int anzahlAndroiden;
	private String raumschiffName;
	private static ArrayList<String> broadCastCommunicator = new ArrayList<String>();
	private ArrayList<String> logbucheintraege;
	private ArrayList<Ladung> ladungRaumschiff;

	
	public Raumschiff(int anzahlTorpedoes, int energieversorgungInProzent, int huelleInProzent, int schildeInProzent,
			int lebenserhaltungssystemeInProzent, int anzahlAndroiden, String raumschiffName) {

		setAnzahlTorpedoes(anzahlTorpedoes);
		setEnergieversorgungInProzent (energieversorgungInProzent);
		setHuelleInProzent(huelleInProzent);
		setSchutzschildInProzent(schutzschildInProzent);
		setLebenserhaltungssystemeInProzent(lebenserhaltungssystemeInProzent);
		setAnzahlAndroiden(anzahlAndroiden);
		setRaumschiffName(raumschiffName);
		this.logbucheintraege = new ArrayList<String>();
		this.ladungRaumschiff = new ArrayList<Ladung>();
	}

	public String getRaumschiffName() {
		return raumschiffName;
	}

	public void setRaumschiffName(String raumschiffName) {
		this.raumschiffName = raumschiffName;
	}
	
	public int getSchutzschildInProzent() {
		return schutzschildInProzent;
	}

	public void setSchutzschildInProzent(int schutzschildInProzent) {
		this.schutzschildInProzent = schutzschildInProzent;
	}
	
	public int getEnergieversorgungInProzent () {
		return energieversorgungInProzent;
	}

	public void setEnergieversorgungInProzent (int energieversorgungInProzent) {
		this.energieversorgungInProzent = energieversorgungInProzent;
	}

	public int getHuelleInProzent() {
		return huelleInProzent;
	}

	public void setHuelleInProzent(int huelleInProzent) {
		this.huelleInProzent = huelleInProzent;
	}

	public int getLebenserhaltungssystemeInProzent() {
		return lebenserhaltungssystemeInProzent;
	}

	public void setLebenserhaltungssystemeInProzent(int lebenserhaltungssystemeInProzent) {
		this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
	}

	public int getAnzahlTorpedoes() {
		return anzahlTorpedoes;
	}

	public void setAnzahlTorpedoes(int anzahlTorpedoes) {
		anzahlTorpedoes = anzahlTorpedoes;
	}

	public int getAnzahlAndroiden() {
		return anzahlAndroiden;
	}

	public void setAnzahlAndroiden(int anzahlAndroiden) {
		this.anzahlAndroiden = anzahlAndroiden;
	}

	public ArrayList<Ladung> getLadungRaumschiff() {
		return ladungRaumschiff;
	}

	public void setLadungRaumschiff(ArrayList<Ladung> ladungRaumschiff) {
		this.ladungRaumschiff = ladungRaumschiff;
	}

	public void ladungHinzufuegen(Ladung sachen) {
		this.ladungRaumschiff.add(sachen);
	}

	public void feuerTorpedoes(Raumschiff Schiff) {
		if (this.anzahlTorpedoes > 0) {
			System.out.println("Photonentorpedos abgeschossen");
			treffer(Schiff);
			this.anzahlTorpedoes = -1;
		} else {
			System.out.println("-=*Click*=-");
		}
	}

	public void feuerPhaser(Raumschiff Schiff) {
		if (this.energieversorgungInProzent > 49) {
			System.out.println("Photonentorpedos abgeschossen");
			treffer(Schiff);
			this.energieversorgungInProzent = -50;
		} else {
			System.out.println("-=*Click*=-");
		}
	}

	private void treffer(Raumschiff Schiff) {
		System.out.println("" + Schiff.raumschiffName + " wurde getroffen!");
		if (Schiff.schutzschildInProzent > 49) {
			Schiff.schutzschildInProzent = -50;
		} else {
			Schiff.huelleInProzent = -50;
			Schiff. energieversorgungInProzent = -50;
			if (Schiff.huelleInProzent < 0) {
				System.out.println("" + Schiff.raumschiffName + "s Lebenserhaltene Systeme wurden zerst�rt!");
				Schiff.lebenserhaltungssystemeInProzent = 0;
			}
		}

	}

	public void nachrichtAnAlle(String Nachricht) {
		System.out.println("" + Nachricht + " wurde im broadcastKommunikator hinzugef�gt!");
		this.broadCastCommunicator.add(Nachricht);
	}

	public ArrayList<String> Logbucheintrag() {
		return this.logbucheintraege;
	}

	public void raumschiffReperatur(Boolean Schutzschild, Boolean Huelle, Boolean Energie, int AndroidenAnzahl) {
		if (AndroidenAnzahl <= this.anzahlAndroiden) {
			int zufall;
			int reperaturwert;
			zufall = (int) (Math.random() * 100);
			if (Schutzschild & Huelle & Energie) {

				reperaturwert = zufall * AndroidenAnzahl / 3;
				this.huelleInProzent = +reperaturwert;
				this.schutzschildInProzent = +reperaturwert;
				this.energieversorgungInProzent = +reperaturwert;
			} else if (Schutzschild & Huelle) {
				reperaturwert = zufall * AndroidenAnzahl / 2;
				this.huelleInProzent = +reperaturwert;
				this.schutzschildInProzent = +reperaturwert;

			} else if (Huelle & Energie) {
				reperaturwert = zufall * AndroidenAnzahl / 2;
				this.huelleInProzent = +reperaturwert;
				this.energieversorgungInProzent = +reperaturwert;
			} else if (Huelle & Energie) {
				reperaturwert = zufall * AndroidenAnzahl / 2;
				this.huelleInProzent = +reperaturwert;
				this.energieversorgungInProzent = +reperaturwert;
			} else if (Huelle) {
				reperaturwert = zufall * AndroidenAnzahl;
				this.huelleInProzent = +reperaturwert;
			} else if (Energie) {
				reperaturwert = zufall * AndroidenAnzahl;
				this.energieversorgungInProzent = +reperaturwert;
			} else if (Schutzschild) {
				reperaturwert = zufall * AndroidenAnzahl;
				this.schutzschildInProzent = +reperaturwert;

			}
		} else {
			System.out.println(" Nicht genug Reperaturandroiden vorhanden!");
		}

	}

	public void statusRaumschiff() {
		System.out.println("Raumschiffname:" + this.raumschiffName);
		System.out.println("Energie:" + this.energieversorgungInProzent + "%");
		System.out.println("H�lle:" + this.huelleInProzent + "%");
		System.out.println("Schutzschild:" + this.schutzschildInProzent + "%");
		System.out.println("Lebenserhaltene Systeme:" + this.lebenserhaltungssystemeInProzent + "%");
		System.out.println("Torpedoes:" + this.anzahlTorpedoes);
		System.out.println("Androiden:" + this.anzahlAndroiden);
	}

	public void statusLadung() {
		System.out.println("" + this.raumschiffName + " enth�lt:");
		for (int i = 0; i < this.ladungRaumschiff.size(); i++)
			System.out.println(this.ladungRaumschiff.get(i));
	}

}
